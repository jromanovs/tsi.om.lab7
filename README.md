### Practice 7. Linear Programming: Software Implementation of the Simplex algorithm

The implementation of the Simplex algorithm for the linear programming (LP) problem in the standard form:

    𝑍 = c1*x1 + c2*x2 + ... + cn * xn -> max,
    a11 * x1 + a12 * x2 + ... + a1n* xn <= b2,
    a21 * x1 + a22 * x2 + ... + s2n *xn <= b2,
    ...
    am1 * x1 + am2 * x2 + ... + amn * xn <= bm,
    x1, x2,..., xn >= 0

or, in the matrix form:

    Z = cx -> max
    Ax <= b

Structure of the input file:

    c1 c2 c3 ... cn
    //empty line
    a11 a12 a13 ... a1n
    a21 a22 a23 ... a2n
    ...
    am1 am2 am3 ... amn
    //empty line
    b1 b2 b3 ... bm

# How to use

Require __python 3.6__ or above


    python main.py [inputfile] [outputfile]

# Testing

    python test.py

# Available at

	git clone https://jromanovs@bitbucket.org/jromanovs/tsi.om.lab7.git


