# Copyright (c) 2021 Jurijs Romanovs yuri@romanov.dev
# See LICENSE.txt for details (MIT License)

from pathlib import Path
from fractions import Fraction


def load_file_sample(filename):
    text = Path(filename).read_text()
    c, a, b = text.split('\n\n')
    b = [Fraction(v) for v in b.split('\t')]
    c = [Fraction(v) for v in c.split('\t')]
    a = [[Fraction(v) for v in e.split('\t')] for e in a.split('\n')]
    return a, b, c
