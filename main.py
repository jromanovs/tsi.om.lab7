#!/usr/bin/env python
# Copyright (c) 2021 Jurijs Romanovs yuri@romanov.dev
# See LICENSE.txt for details (MIT License)

import sys
from simplex import solve_simplex
from pathlib import Path
from file_parsers import load_file_sample


def main():
    if len(sys.argv) != 3:
        print(f'ERROR: Wrong number of input parameters', file=sys.stderr)
        exit(-1)

    if not Path(sys.argv[1]).exists():
        print(f'ERROR: Input file {sys.argv[1]} does not exists', file=sys.stderr)
        exit(-2)

    try:
        with open(sys.argv[2], 'w') as f:
            try:
                a, b, c = load_file_sample(sys.argv[1])
                solve_simplex(a, b, c, f)

            except Exception as e:
                print(f'ERROR: {" ".join(e.args)}', file=f)
    except Exception as e:
        print(f'{e.args}', file=sys.stderr)


if __name__ == '__main__':
    main()
