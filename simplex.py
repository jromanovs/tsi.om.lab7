# Copyright (c) 2021 Jurijs Romanovs yuri@romanov.dev
# See LICENSE.txt for details (MIT License)

TAB = '\t'


def solve_simplex(a, b, c, output_file):
    """
    Solve linear programming optimization problem using the Simplex method

    :param a: the table with shape (n,m) where n number of equations and
              m is number of variables including
    :param b: constraint constants
    :param c: basic variables coefficients with size of m
    :param output_file: open file handle for a result output
    :return: (roots, z-value)
    """
    validate_simplex(a, b, c)
    table, basis = canonic_form(a, b, c)
    print_solution(table, basis, output_file)
    pivot_columns_used = []
    roots = []
    while True:
        pivot_column = find_pivot_column(table[-1][:-1], basis, pivot_columns_used)
        if pivot_column is None:
            break
        pivot_row = find_pivot_row(table, basis, pivot_column)
        if pivot_row is None:
            break
        table, basis, roots = simplex_next(table, basis, roots, pivot_column, pivot_row)
        print_solution(table, basis, output_file)
        # guard to avoid cycling
        if len(roots) > 100:
            roots = []
            break
    if len(roots) > 1:
        print('SOLUTION FOUND: multiple solutions', file=output_file)
    elif len(roots) == 1:
        print('SOLUTION FOUND: unique solution', file=output_file)
    else:
        print('NO SOLUTION: unbounded problem', file=output_file)
        # or not converged problem?
    print(f'Objective: z = {table[-1][-1]}', end='', file=output_file)
    for root in roots:
        print(f"\n{TAB.join([f'{i}' for i in root])}", end='\t', file=output_file)
    return roots, table[-1][-1]


def validate_simplex(a, b, c):
    assert len(a) > 0, "incorrect input data"
    assert len(b) == len(a), "incorrect input data"
    assert len(c) == len(a[0]), "incorrect input data"
    assert min(b) >= 0, "incorrect problem statement"


def canonic_form(a, b, c):
    return [
        [row + identity_vector(b, i) + [b[i]] for i, row in enumerate(a)]
        + [[-x for x in c] + [0]*(len(b)+1)],
        [x+len(c) for x in range(len(b))]
    ]


def identity_vector(b, i):
    return [int(x == i) for x in range(len(b))]


def print_solution(a, basis, file):
    print('', end=TAB, file=file)
    for i in range(len(a[0])-1):
        print_label(i, a, basis, file=file)
    print('b', file=file)
    for i in range(len(basis)):
        print_label(basis[i], a, basis, file)
        print(TAB.join([f'{x}' for x in a[i]]), end='\t\n', file=file)
    print('c', end=TAB, file=file)
    print(TAB.join([f'{x}' for x in a[-1]]), end='\t\n', file=file)
    print(file=file)


def print_label(index, a, basis, file):
    if index < x_len(a, basis):
        print(f'x{index + 1}', end=TAB, file=file)
    else:
        print(f's{index - x_len(a, basis) + 1}', end=TAB, file=file)


def find_pivot_column(last_row_wo_z, basis, used):
    x_size = len(last_row_wo_z) - len(basis)
    pivot_column = find_pivot_column_in_basic_variables(last_row_wo_z, basis, used, x_size)
    if pivot_column is None:
        pivot_column = find_pivot_column_in_slack_variables(last_row_wo_z, basis, used, x_size)
    if pivot_column is not None:
        used += [pivot_column]
    return pivot_column


def find_pivot_column_in_basic_variables(last_row_wo_z, basis, used, x_size):
    pivot_column = None
    min_value = 0
    for i_x, ci in enumerate(last_row_wo_z[:x_size]):
        # Can ci be a None? (+inf)
        if (i_x not in basis) and (pivot_column is None or ci <= min_value) and i_x not in used:
            min_value = ci
            pivot_column = i_x
    return pivot_column


def find_pivot_column_in_slack_variables(last_row_wo_z, basis, used, x_size):
    min_value = 0
    pivot_column = None
    for i_slack, ci in enumerate(last_row_wo_z[x_size:]):
        i = i_slack + x_size
        # Can ci be a None? (+inf)
        if (i not in basis) and (pivot_column is None or ci <= min_value) and (i not in used):
            min_value = ci
            pivot_column = i
    if last_row_wo_z[pivot_column] > 0:
        pivot_column = None
    return pivot_column


def find_pivot_row(table, basis, pivot_column):
    pivot_row = None
    min_value = None
    for i_row, row in enumerate(table[:-1]):
        if row[pivot_column] != 0 and basis[i_row] >= x_len(table, basis) and basis[i_row] != pivot_column:
            b_over_a = row[-1] / row[pivot_column]
            if b_over_a > 0 and (min_value is None or b_over_a < min_value):
                min_value = b_over_a
                pivot_row = i_row
    return pivot_row


def x_len(a, basis):
    return len(a[0])-len(basis)-1


def simplex_next(table, basis, roots, col, row):
    pivot = table[row][col]
    previous_z = table[-1][-1]
    basis[row] = col
    for i, line in enumerate(table):
        if i != row:
            for j, x in enumerate(line):
                if j not in basis:
                    if table[row][col] == 0:
                        table[i][j] = None
                    else:
                        table[i][j] = table[i][j] - (table[row][j] * table[i][col] / table[row][col])
    normalize_pivot_row(table[row], pivot)
    roots = assign_roots(table, basis, roots, previous_z)
    fill_identity_for_basis(table, basis)
    return table, basis, roots


def normalize_pivot_row(row, pivot):
    for row_index in range(len(row)):
        row[row_index] = row[row_index] / pivot


def assign_roots(table, basis, roots, previous_z):
    if table[-1][-1] >= previous_z:
        if table[-1][-1] > previous_z:
            roots = []
        new_roots = [0] * (len(table[0]) - 1)
        for column_index in range(len(table) - 1):
            new_roots[basis[column_index]] = table[column_index][-1]
        if new_roots not in roots and min(new_roots) >= 0:
            roots.append(new_roots)
    return roots


def fill_identity_for_basis(table, basis):
    for constraint_index in range(len(basis)):
        column_in_basis_index = basis[constraint_index]
        for row_index in range(len(table)):
            if row_index == constraint_index:
                table[row_index][column_in_basis_index] = 1
            else:
                table[row_index][column_in_basis_index] = 0
