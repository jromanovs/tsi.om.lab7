#!/usr/bin/env python
# Copyright (c) 2021 Jurijs Romanovs yuri@romanov.dev
# See LICENSE.txt for details (MIT License)
from unittest import TestCase, main
from file_parsers import load_file_sample
from tempfile import TemporaryFile
from pathlib import Path
from fractions import Fraction
from simplex import *


class TestParsers(TestCase):
    def __sample_file_test(self, filename_pattern, roots):
        with TemporaryFile("w+") as f:
            a, b, c = load_file_sample(f'fixtures/input{filename_pattern}.txt')
            self.assertEqual(roots, solve_simplex(a, b, c, f))
            f.seek(0)
            text = ''.join(f.readlines())
            self.assertEqual(Path(f'fixtures/output{filename_pattern}.txt').read_text(), text)

    def test_basic_data_load(self):
        a, b, c = load_file_sample('fixtures/input0.txt')
        self.assertEqual([1, 2, 1], c)
        self.assertEqual([2, 6, 6], b)
        self.assertEqual([[2, 1, -1], [2, -1, 5], [4, 1, 1]], a)

    def test_provided_sample(self):
        self.__sample_file_test('0', ([[0, 4, 2, 0, 0, 0]], 10))

    def test_simplex_auto_assembly(self):
        self.__sample_file_test('_bonus', ([[3800, 2400, 0, 0, 1100]], 26640000))

    def test_simplex_p6t1(self):
        self.__sample_file_test('_p6t1', ([[Fraction(100, 3), Fraction(250, 3), 0, 0]], Fraction(95000, 3)))

    def test_simplex_p5_1(self):
        self.__sample_file_test('_p5t1', ([[3200, 1200, 80, 0, 0]], 2920))

    def test_simplex_p5_2(self):
        self.__sample_file_test('_p5t2',
                                ([[Fraction('9/7'), Fraction('40/7'), 0, 0, Fraction('165/7')]], Fraction('129/7')))

    def test_simplex_p5_3(self):
        self.__sample_file_test('_p5t3', ([[0, 4, 2, 0, 0, 0]], 10))

    def test_simplex_p4_2(self):
        self.__sample_file_test('_p4t2', ([
                     [Fraction(63, 11), Fraction(42, 11), Fraction(102, 11), 0, 0], [15, 1.5, 0, 0, Fraction(51, 10)]
                 ], 1050))

    def test_multiple(self):
        self.__sample_file_test('_multiple', ([
                     [0, Fraction(100, 9), 0, Fraction(80, 9)], [Fraction(20, 3), Fraction(20, 3), 0, 0],
                 ], Fraction(100000, 3)))

    def test_unbounded(self):
        self.__sample_file_test('_unbounded', ([], 0))


if __name__ == '__main__':
    main()
